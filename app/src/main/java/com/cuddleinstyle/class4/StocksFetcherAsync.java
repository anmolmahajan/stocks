package com.cuddleinstyle.class4;

import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by anmol on 12/9/15.
 */
public class StocksFetcherAsync extends AsyncTask<String, Void, Stock[]> {

    StockInterface listener;

    @Override
    protected void onPostExecute(Stock[] stock){
        if(listener != null){
            listener.StocksTaskOnComplete(stock);
        }
    }
    @Override
    protected Stock[] doInBackground(String... params){
        String urlString = params[0];
        try{
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream data = urlConnection.getInputStream();
            Scanner s = new Scanner(data);
            StringBuffer output = new StringBuffer();
            while (s.hasNext()) {
                output.append(s.nextLine());
            }
            s.close();
            urlConnection.disconnect();
            return parseJson(output.toString());
        }catch(MalformedURLException e){
            return null;
        }catch(IOException e){
            return null;
        }


    }
    private Stock[] parseJson(String jsonString){
        try{

            JSONObject object = new JSONObject(jsonString);
            JSONObject query = object.getJSONObject("query");
            JSONObject results = query.getJSONObject("results");
            Stock[] output;
            if(query.getInt("count") == 1) {
                JSONObject quotes = results.getJSONObject("quote");
                output = new Stock[1];
                Stock s = new Stock();
                s.name = quotes.getString("Name");
                s.handle = quotes.getString("symbol");
                String original = quotes.getString("Ask");
                String change = quotes.getString("Change");
                s.value = original + "/" + change;
                output[0] = s;
            }else if(query.getInt("count") > 1) {
                JSONArray quotes = results.getJSONArray("quote");
                output = new Stock[quotes.length()];
                for (int i = 0; i < quotes.length(); i++) {
                    JSONObject company = quotes.getJSONObject(i);
                    Stock s = new Stock();
                    s.name = company.getString("Name");
                    s.handle = company.getString("symbol");
                    String original = company.getString("Ask");
                    String change = company.getString("Change");
                    s.value = original + "/" + change;
                    output[i] = s;
                }
            }else{
                output = null;
            }
            return output;
        }catch(JSONException e){
            return null;
        }
    }
}
