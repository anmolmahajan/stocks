package com.cuddleinstyle.class4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by anmol on 5/9/15.
 */
public class CustomAdapter extends ArrayAdapter<Stock>{
    Context context;
    public CustomAdapter(Context context, List<Stock> data){
        super(context,0,data);
        this.context = context;
    }

    public class ViewHolder{
        TextView name;
        TextView handle;
        TextView value;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView==null){
            convertView = View.inflate(context,R.layout.custom_layout,null);
            ViewHolder vh = new ViewHolder();
            vh.name = (TextView)convertView.findViewById(R.id.name);
            vh.handle = (TextView)convertView.findViewById(R.id.handle);
            vh.value = (TextView)convertView.findViewById(R.id.value);
            convertView.setTag(vh);
        }
            ViewHolder vh = (ViewHolder) convertView.getTag();
            Stock s = getItem(position);

            vh.handle.setText(s.handle + "");
            vh.name.setText(s.name+"");
            vh.value.setText(s.value + "");

            return convertView;

    }

}
