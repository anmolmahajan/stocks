package com.cuddleinstyle.class4;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,StockInterface{

    ListView lv;
    ArrayList<Stock> data;
    ArrayList<String> ticker;
    CustomAdapter adapter;
    LayoutInflater inflater;
    ProgressDialog pd;
    AutoCompleteTextView autocomplete;
    Context app;
    String[] arr = {"NUGT","AAPL","VXX","BAC","PBR","AVP","MRVL","FCX"
                        ,"FTR","VVUS","MSFT","GOOG","FB", "C","HPQ","INTC",
                        "KO","LUV","T","TXN","WMT","AEL","F","B","S","V",
                        "SNE","HMC","MS","GS","GM","VZ"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        data = new ArrayList<>();
        ticker = new ArrayList<>();
        getbackupdata();
        getdata();
        lv = (ListView) findViewById(R.id.lv);
        app = this;                             //For toast context
        inflater = getLayoutInflater();
        adapter = new CustomAdapter(this,data);        //For listview
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,final int position, long id) {
                AlertDialog.Builder b = new AlertDialog.Builder(MainActivity.this);
                b.setTitle("Delete?");
                b.setMessage("Are you sure you want to delete the entry for " +
                        adapter.getItem(position).handle.toString() + " ?");
                b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ticker.remove(position);
                        data.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });
                b.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                b.show();

            }
        });

    }

    public void getdata(){
        String urlString = getURLString();
        StocksFetcherAsync sfa = new StocksFetcherAsync();
        sfa.listener = this;
        Log.i("urlstring", urlString);
        sfa.execute(urlString);
        pd = new ProgressDialog(MainActivity.this);
        pd.setTitle("Fetching");
        pd.setMessage("Fetching the data. Please wait.");
        pd.show();
    }

    public String getURLString(){
        String s = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20" +
                    "from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22";


        if(ticker == null || ticker.size() == 0) {
            ticker.add("MSFT");
        }
        for (int i = 0; i < ticker.size() - 1; i++) {
            s += ticker.get(i) + "%22%2C%22";
        }
        s += ticker.get(ticker.size() - 1);


        s += "%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
        Log.i("output", s);
        return s;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.edit){
            AlertDialog.Builder b = new AlertDialog.Builder(MainActivity.this);
            b.setTitle("Select");
            View v = inflater.inflate(R.layout.adder_layout,null);

            autocomplete = (AutoCompleteTextView)
                    v.findViewById(R.id.search_box);
            final ArrayAdapter<String> ad = new ArrayAdapter<String>
                    (this,android.R.layout.select_dialog_item, arr);
            autocomplete.setThreshold(1);
            autocomplete.setAdapter(ad);
            autocomplete.setMaxHeight(250);
            b.setView(v);
            final ArrayList<String> tempticker = new ArrayList<>();
            autocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    tempticker.add((String)parent.getItemAtPosition(position));
                    autocomplete.setText("");
                }
            });

            b.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(tempticker.size() != 0) {
                        ticker.addAll(tempticker);
                    }
                    getdata();
                }
            });

            b.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.show();


        }
        else if(id == R.id.refresh){
            getdata();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onStop() {
        try {
            FileOutputStream output = openFileOutput("tickers.txt", MODE_PRIVATE);
            DataOutputStream dout = new DataOutputStream(output);
            dout.writeInt(ticker.size()); // Save line count
            for(int i = 0; i < ticker.size(); i++){
                dout.writeUTF(ticker.get(i));
            }
            dout.flush(); // Flush stream ...
            dout.close(); // ... and close.
        }catch (IOException e){ e.printStackTrace(); };
        super.onStop();
    }

    @Override
    public void StocksTaskOnComplete(Stock[] stock) {
        if(stock != null){
            data.clear();
            for (Stock s : stock) {
                data.add(s);
            }
        }
        adapter.notifyDataSetChanged();
        pd.dismiss();
    }

    public void getbackupdata(){
        try {
            FileInputStream input = openFileInput("tickers.txt"); // Open input stream
            DataInputStream din = new DataInputStream(input);
            int sz = din.readInt(); // Read line count
            for (int i = 0; i < sz; i++) { // Read lines
                ticker.add(din.readUTF());
            }
            din.close();
        }catch (IOException e){ e.printStackTrace(); }
    }
}
